package kwetter.microservice.logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoggerApi
{
    public static void main(String[] args)
    {
        SpringApplication.run(LoggerApi.class, args);
    }
}