package kwetter.microservice.logger.controller;

import kwetter.microservice.logger.configurations.RabbitConstraints;
import kwetter.microservice.logger.entity.Log;
import kwetter.microservice.logger.service.LoggingService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class LoggingListener implements RabbitConstraints
{
    private final LoggingService logService;

    @RabbitListener(queues = QUEUE_ADD_LOG)
    public void receiveAddedUser(Log log)
    {
        System.out.println("Error level: " + log.getLevel());
        System.out.println("Error origin: " + log.getOrigin());
        System.out.println("Error content: " + log.getContent());
        System.out.println("Error time: " + new Date(log.getTime()));
        logService.addNew(log);
    }
}