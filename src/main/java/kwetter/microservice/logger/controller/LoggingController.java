package kwetter.microservice.logger.controller;

import kwetter.microservice.logger.entity.Log;
import kwetter.microservice.logger.service.LoggingService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/logs")
@RequiredArgsConstructor
public class LoggingController
{
    private final LoggingService loggingService;

    @GetMapping("/")
    public ResponseEntity<List<Log>> getAll()
    {
        System.out.println("get all received");
        return ResponseEntity.ok().body(loggingService.getAll());
    }
}