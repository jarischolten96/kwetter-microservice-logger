package kwetter.microservice.logger.service;

import kwetter.microservice.logger.entity.Log;
import kwetter.microservice.logger.repository.LoggingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LoggingService
{
    private final LoggingRepository loggingRepository;

    public void addNew(final Log event)
    {
        loggingRepository.save(event);
    }

    public List<Log> getAll()
    {
        List<Log> logs = new ArrayList<>();
        loggingRepository.findAll().forEach(logs::add);
        return logs;
    }
}