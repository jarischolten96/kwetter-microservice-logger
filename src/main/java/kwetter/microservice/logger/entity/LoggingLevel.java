package kwetter.microservice.logger.entity;

public enum LoggingLevel
{
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    CRITICAL
}
