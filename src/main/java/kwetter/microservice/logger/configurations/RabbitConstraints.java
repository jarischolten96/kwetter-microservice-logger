package kwetter.microservice.logger.configurations;

public interface RabbitConstraints
{
    String QUEUE_ADD_LOG = "queue-add-event";
    String EXCHANGE_ADD_LOG = "exchange-add-event";
    String ROUTING_KEY = "routingkey";
}