package kwetter.microservice.logger.repository;

import kwetter.microservice.logger.entity.Log;
import org.springframework.data.repository.CrudRepository;
import java.util.UUID;

public interface LoggingRepository extends CrudRepository<Log, UUID>
{

}